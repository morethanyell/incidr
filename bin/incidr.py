#!/usr/bin/env python

import sys, re
from math import log10
from splunklib.searchcommands import \
    dispatch, StreamingCommand, Configuration, Option, validators


@Configuration()
class inCIDRCommand(StreamingCommand):

    cidr = Option(
        doc='''
        **Syntax:** **cidrblock=***<valid_cidr_block>*
        **Description:** A valid CIDR block string, e.g. 10.0.0.0/24.''',
        require=True)

    field = Option(
        doc='''
        **Syntax:** **input=***<existing_fieldname>*
        **Description:** Name of the field containing the values will be matched against the resulting regex from the CIDR block given.''',
        require=True, validate=validators.Fieldname())

    remove = Option(
        doc='''
        **Syntax:** **remove=***<true|false>*
        **Description:** Filters out the event when input matches the CIDR block as provided. Default is true.''',
        require=False, default=True, validate=validators.Boolean())

    show_regex = Option(
        doc='''
        **Syntax:** **show_regex=***<true|false>*
        **Description:** Adds an extra field showing the regex patter when set to true. Defalt is false.''',
        require=False, default=False, validate=validators.Boolean())

    regex_fieldname = Option(
        doc='''
        **Syntax:** **regex_fieldname=***<true|false>*
        **Description:** Name of the field that will contain the regex result.''',
        require=False, default="regex_pattern")

    def stream(self, events):
        rfn = self.regex_fieldname if (self.regex_fieldname != "") else "regex_pattern"
        cidrblocks = [x.strip() for x in self.cidr.split(',')]

        matched = []
        mismatched = []

        for event in events:
            is_matched = False
            for cidrblock in cidrblocks:
                regex = self.cidr_to_regex(cidrblock)
                is_matched = re.search(regex, event[self.field])
                if (is_matched):
                    if (self.show_regex): event[rfn] = regex
                    matched.append(event)
                    break
            if not (is_matched):
                if (self.show_regex): event[rfn] = ""
                mismatched.append(event)

        if (self.remove):
            for e in mismatched: yield e
        else:
            for e in matched: yield e

    @staticmethod
    def cidr_to_regex(cidr):
        ip, prefix = cidr.split('/')
        base = 0

        for val in map(int, ip.split('.')):
            base = (base << 8) | val

        shift = 32 - int(prefix)
        start = base >> shift << shift
        end = start | (1 << shift) - 1

        def regex(lower, upper):
            if lower == upper:
                return str(lower)

            exp = int(log10(upper - lower))

            if (int(str(lower)[-1]) > int(str(upper)[-1]) and exp == 0):
                # increasing exp due to base 10 wrap to next exp
                exp += 1
            delta = 10 ** exp

            if lower == 0 and upper == 255:
                return "\d+"

            if delta == 1:
                val = ""
                for a, b in zip(str(lower), str(upper)):
                    if a == b:
                        val += str(a)
                    elif (a, b) == ("0", "9"):
                        val += '\d'
                    elif int(b) - int(a) == 1:
                        val += '[%s%s]' % (a, b)
                    else:
                        val += '[%s-%s]' % (a, b)
                return val

            def gen_classes():
                def floor_(x):
                    return int(round(x / delta, 0) * delta)

                xs = range(floor_(upper) - delta, floor_(lower), -delta)
                for x in map(str, xs):
                    yield '%s%s' % (x[:-exp], r'\d' * exp)

                    yield regex(lower, floor_(lower) + (delta - 1))
                    yield regex(floor_(upper), upper)
            return "(%s)" % '|'.join(gen_classes())

        def get_parts():
            for x in range(24, -1, -8):
                yield regex(start >> x & 255, end >> x & 255)

        # Not using the typical r'\.' regex because Splunk automatically
        # replaces "." with "\." for the whitelist and blacklist entries.
        return '^%s$' % '.'.join(get_parts())


dispatch(inCIDRCommand, sys.argv, sys.stdin, sys.stdout, __name__)
